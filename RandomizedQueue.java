/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */


import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {

    private Item[] queue;

    private int size;

    // construct an empty randomized queue
    public RandomizedQueue() {
        queue = (Item[]) new Object[1];
        size = 0;
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new RandomArrayIterator();
    }

    private class RandomArrayIterator implements Iterator<Item> {
        private int iteratorSize;

        private Item[] iteratorElements;

        public RandomArrayIterator() {
            iteratorElements = (Item[]) new Object[queue.length];
            for (int i = 0; i < queue.length; i++) {
                iteratorElements[i] = queue[i];
            }
            iteratorSize = size;
        }

        public boolean hasNext() {
            return iteratorElements[0] != null;
        }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            int index = StdRandom.uniform(0, iteratorSize);
            Item item = iteratorElements[index];
            if (iteratorSize > 1 && index < iteratorSize - 1) {
                iteratorElements[index] = iteratorElements[iteratorSize - 1];
                iteratorElements[iteratorSize - 1] = null;
            }
            else {
                iteratorElements[index] = null;
            }
            iteratorSize--;
            return item;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    // is the randomized queue empty?
    public boolean isEmpty() {
        return size == 0;
    }

    // return the number of items on the randomized queue
    public int size() {
        return size;
    }

    // add the item
    public void enqueue(Item item) {
        if (item == null) throw new IllegalArgumentException();
        if (size == queue.length) resize(2 * queue.length);
        queue[size++] = item;
    }

    private void resize(int capacity) {
        Item[] copy = (Item[]) new Object[capacity];
        for (int i = 0; i < size; i++) {
            if (queue[i] != null) {
                copy[i] = queue[i];
            }
        }
        queue = copy;
    }

    // remove and return a random item
    public Item dequeue() {
        if (isEmpty()) throw new NoSuchElementException();
        int index = StdRandom.uniform(0, size);
        Item item = queue[index];
        if (size > 1 && index < size - 1) {
            queue[index] = queue[size - 1];
            queue[size - 1] = null;
        }
        else queue[index] = null;
        size--;
        if (size > 0 && size == queue.length / 4) resize(queue.length / 2);
        return item;
    }

    // return a random item (but do not remove it)
    public Item sample() {
        if (isEmpty()) throw new NoSuchElementException();
        return queue[StdRandom.uniform(0, size)];
    }


    // unit testing
    public static void main(String[] args) {
        RandomizedQueue<Integer> q = new RandomizedQueue<>();
        q.enqueue(1);
        System.out.println(q.isEmpty());
        System.out.println(q.sample());
        System.out.println(q.size);
        System.out.println(q.dequeue());
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        for (Object a : q
        ) {
            System.out.println(a);
        }
    }
}
