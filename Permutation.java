/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.StdIn;

public class Permutation {
    public static void main(String[] args) {
        int k = Integer.parseInt(args[0]);
        String s;
        RandomizedQueue<String> q = new RandomizedQueue<>();
        while (!StdIn.isEmpty()) {
            s = StdIn.readString();
            q.enqueue(s);
        }
        int i = 0;
        for (Object e : q
        ) {
            if (i >= k) break;
            System.out.println(e);
            i++;
        }
    }
}
