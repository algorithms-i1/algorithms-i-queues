# Algorithms I Assignment 2: Queues
Solution for assignment 2 of the Princeton University Algorithms I course on Coursera (https://coursera.cs.princeton.edu/algs4/assignments/queues/specification.php).

Includes implementations of a deque/double-ended queue as well as a randomized queue, where dequeue and iterate operations return elements in a random order.

## Results  

Correctness:  49/49 tests passed  

Memory:       124/124 tests passed  

Timing:       193/193 tests passed  

<b>Aggregate score:</b> 100.00%  


## How to use
The Permutation.java file can be compiled with the javac command or an IDE and run to showcase the behaviour of RandomizedQueue. Both Deque.java and RandomizedQueue.java also include main methods that showcase their functionality.
Requires the algs4.jar library used in the course.
